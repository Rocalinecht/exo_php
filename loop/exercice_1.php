<?php   
////____________________________________EXERCICE 1
// #PHP - Les boucles 
// ##Exercice 1 Créer une variable et l'initialiser à 0.
// Tant que cette variable n'atteint pas 10, il faut : - l'afficher
//                                                     - l'incrementer 

$init1 = 0;
for ($init1 = 0; $init1 <= 10; $init1++) {
    echo $init1 . " ";
};